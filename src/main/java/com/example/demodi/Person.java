package com.example.demodi;

import org.springframework.stereotype.Component;

@Component
public class Person {

    private String imie;
    private String nazwisko;

    public Person() {
    }

    public Person(String imie, String nazwisko) {
        this.imie = imie;
        this.nazwisko = nazwisko;
    }

    @Override
    public boolean equals(Object o) {
        System.out.println("execute equals");
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Person person = (Person) o;

        if (imie != null ? !imie.equals(person.imie) : person.imie != null) return false;
        return nazwisko != null ? nazwisko.equals(person.nazwisko) : person.nazwisko == null;
    }

    @Override
    public int hashCode() {
        System.out.println("execute hashcode");
        int result = imie != null ? imie.hashCode() : 0;
        result = 31 * result + (nazwisko != null ? nazwisko.hashCode() : 0);
        return result;
        //return 100;
    }


    @Override
    public String toString() {
        return "Person{" +
                "imie='" + imie + '\'' +
                ", nazwisko='" + nazwisko + '\'' +
                '}';
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }
}
