package com.example.demodi;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class BannerDisplay {

    private String id = "";
    //@Autowired
    private MessageProducer messageProducer;
    //@Autowired
    private Person person;

    @Autowired
    public BannerDisplay(@Qualifier("TEMPERATURE") MessageProducer messageProducer, Person person) {
        this.messageProducer = messageProducer;
        this.person = person;
    }
//    public BannerDisplay(String id, MessageProducer messageProducer) {
//        this.id = id;
//        this.messageProducer = messageProducer;
//    }

    public void refresh(){
        System.out.println("BannerDisplay " + person + ": "
                + messageProducer.getMessage());
    }

}
