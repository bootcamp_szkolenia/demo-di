package com.example.demodi;

public interface MessageProducer {
    String getMessage();

    // stwórz klasę implementująca ten interfejs, zwracającą
    // aktualną datę i czas
    //CurrentDateTimeMessageProducer
}
