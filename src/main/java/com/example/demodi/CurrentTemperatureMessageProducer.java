package com.example.demodi;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.security.SecureRandom;

@Service
@Qualifier("TEMPERATURE")
public class CurrentTemperatureMessageProducer
    implements MessageProducer {

    private SecureRandom secureRandom = new SecureRandom();

    @Override
    public String getMessage() {
        return "Current temperature: " //+ getTempFromOpenWeather()
                + secureRandom.nextInt(20);
    }

    private String getTempFromOpenWeather(){
        return "invoke webservice www.openweather.com/get...";
    }
}
