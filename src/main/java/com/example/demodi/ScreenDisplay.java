package com.example.demodi;

public class ScreenDisplay {
    private String id;
    private MessageProducer messageProducer;

    public ScreenDisplay(String id, MessageProducer messageProducer) {
        this.id = id;
        this.messageProducer = messageProducer;
    }

    public void refresh(){
        System.out.println("ScreenDisplay " + id + ": "
                + messageProducer.getMessage());
    }
}
