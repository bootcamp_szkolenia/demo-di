package com.example.demodi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

@SpringBootApplication
public class DemoDiApplication {

	public static void main(String[] args) {
		ConfigurableApplicationContext context = SpringApplication.run(DemoDiApplication.class, args);

		Person p1 = new Person("J", "P");
		Person p2 = new Person("J", "P");
		Person p3 = new Person("J", "K");

		System.out.println(p1==p2);
		System.out.println(p1.equals(p2));

		Set<Person> personSet = new LinkedHashSet<>();

		personSet.add(p1);
		System.out.println("---");
		personSet.add(p2);
		System.out.println("---");
		personSet.add(p3);

		System.out.println(personSet);

		MessageProducer messageProducer = new CurrentDateTimeMessageProducer();
		System.out.println(messageProducer.getMessage());

//		BannerDisplay bannerDisplay =
//				new BannerDisplay("#1", messageProducer);
//		bannerDisplay.refresh();
//
//		BannerDisplay bannerDisplay2 =
//				new BannerDisplay("#2", new CurrentTemperatureMessageProducer());
//		bannerDisplay2.refresh();

		ScreenDisplay screenDisplay =
				new ScreenDisplay("#1", new CurrentTemperatureMessageProducer());
		screenDisplay.refresh();
		screenDisplay.refresh();

		System.out.println("-------");
		BannerDisplay beanBanner = context.getBean(BannerDisplay.class);
		beanBanner.refresh();


	}
}
