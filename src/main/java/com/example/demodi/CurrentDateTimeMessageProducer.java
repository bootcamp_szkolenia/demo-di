package com.example.demodi;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
@Primary
@Qualifier("DATE_TIME")
public class CurrentDateTimeMessageProducer
        implements MessageProducer
{
    @Override
    public String getMessage() {
        Date date = new Date();
        //System.out.println(date);
        return "Current time: " + date;
    }
}
